Huawei signal monitor
=====================

This is a small ruby script to monitor the signal strength of Huawei's LTE
modems. Currently supports only Huawei E5186s-22a.
