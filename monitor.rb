#!/usr/bin/env ruby

require 'httparty'
require 'nokogiri'

class InvalidSessionIdException < RuntimeError
end

# Parse the 'set-cookie' string
# From http://stackoverflow.com/a/36330054/359059
# @param [String] all_cookies_string
# @return [Hash]
def parse_set_cookie(all_cookies_string)
  cookies = Hash.new

  unless all_cookies_string.empty?
    # single cookies are devided with comma
    all_cookies_string.split(',').each {
      # @type [String] cookie_string
        |single_cookie_string|
      # parts of single cookie are seperated by semicolon; first part is key and value of this cookie
      # @type [String]
      cookie_part_string  = single_cookie_string.strip.split(';')[0]
      # remove whitespaces at beginning and end in place and split at '='
      # @type [Array]
      cookie_part         = cookie_part_string.strip.split('=')
      # @type [String]
      key                 = cookie_part[0]
      # @type [String]
      value               = cookie_part[1]

      # add cookie to Hash
      cookies[key] = value
    }
  end

  cookies
end

def get_session_id
  response = HTTParty.get('http://192.168.8.1/html/index.html')
  cookies = parse_set_cookie(response.headers['set-cookie'])
  cookies['SessionID']
end

def get_signal(session_id)
  response = HTTParty.get(
    'http://192.168.8.1/api/device/signal',
    {
      headers: {
        'Cookie': "SessionID=#{session_id}"
      }
    }
  )

  xml = Nokogiri::XML(response) do |config|
    config.strict.nonet
  end
  error = xml.xpath('/error')
  unless error.empty?
    code = error.xpath('code').first.content.to_i
    if code == 125002
      raise InvalidSessionIdException
    end
  else
    rsrq = xml.xpath('/response/rsrq').first.content
    rsrp = xml.xpath('/response/rsrp').first.content
    sinr = xml.xpath('/response/sinr').first.content
    puts "#{rsrq}   #{rsrp}   #{sinr}"
  end
end

puts "rsrq   rsrp     sinr"
session_id = ''
loop do
  begin
    get_signal(session_id)
    sleep(1)
  rescue InvalidSessionIdException
    session_id = get_session_id
  end
end
